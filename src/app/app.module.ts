import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PenetrationSourceComponent } from './components/penetration-source/penetration-source.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';

import { MatSlideToggleModule, MatTooltipModule, MatButtonModule, MatDialogModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PenetrationSourceInfoComponent } from './components/penetration-source-info/penetration-source-info.component';


@NgModule({
  declarations: [
    AppComponent,
    PenetrationSourceComponent,
    NavBarComponent,
    PenetrationSourceInfoComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatDialogModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [PenetrationSourceInfoComponent]
})
export class AppModule { }
