import { PenetrationSourceInfoComponent } from './../penetration-source-info/penetration-source-info.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PenetrationSourceModel } from './penetration-source.model';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-penetration-source',
  templateUrl: './penetration-source.component.html',
  styleUrls: ['./penetration-source.component.css']
})
export class PenetrationSourceComponent implements OnInit {

  @Input() config: PenetrationSourceModel;
  @Output() updated: EventEmitter<any> = new EventEmitter();

  showOptions = false;
  showInfoIcon = false;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  enabledChanged(e: any) {
    this.config.enabled = e.checked;
    this.updated.emit();
  }

  updateSelection(selection: number) {
    this.config.selected = selection;
    this.updated.emit();
  }

  hasOption() {
    return this.config.amounts.length > 1;
  }

  toggleOptions() {
    this.showOptions = !this.showOptions;
  }

  showInfo() {
    if (this.config.description != null) {
      this.showInfoIcon = true;
    }
  }

  hideInfo() {
    this.showInfoIcon = false;
  }

  showInfoDialog() {
    const dialogRef = this.dialog.open(PenetrationSourceInfoComponent, {
      width: '90%',
      maxWidth: 800,
      data: this.config
    });
  }
}
