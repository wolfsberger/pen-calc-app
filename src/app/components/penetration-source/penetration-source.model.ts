export interface PenetrationSourceModel {
    name: string;
    magica: boolean;
    stamina: boolean;
    selected: number;
    amounts: {name: string, value: number}[];
    enabled: boolean;
    description?: string;
    causedBy?: string[];
}
