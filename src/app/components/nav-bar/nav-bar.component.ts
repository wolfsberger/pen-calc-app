import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  @Input() AppName: string;
  @Output() PenetrationTypeChanged: EventEmitter<any> = new EventEmitter();

  selectedPenetrationSource = 'magicka';

  constructor() { }

  ngOnInit() {
  }

  onChangePenetrationType(type: string) {
    this.PenetrationTypeChanged.emit(type);
  }
}
