import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-penetration-source-info',
  templateUrl: './penetration-source-info.component.html',
  styleUrls: ['./penetration-source-info.component.css']
})
export class PenetrationSourceInfoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PenetrationSourceInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  showCauses() {
    return this.data.causedBy != null;
  }
}
