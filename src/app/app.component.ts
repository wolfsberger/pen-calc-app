import { PenetrationSourceModel } from './components/penetration-source/penetration-source.model';
import { Component, OnInit } from '@angular/core';
import PenetrationSources from './app.penetration-sources';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ESO Penetration Calculator';
  sources = [];
  selectedPenetrationSource = 'magicka';
  typicalBossPenetration = 18200;
  basePenetration = 100;
  totalPenetration = 100;
  remainingBossArmor = 18100;

  ngOnInit() {
    this.getPenetrationSources();
    this.onDataUpdated(0);
  }

  getPenetrationSources() {
    this.sources = [];
    PenetrationSources.forEach(source => {
      if (this.selectedPenetrationSource === 'magicka') {
        if (source.magica) {
          this.sources.push(source);
        }
      } else {
        if (source.stamina) {
          this.sources.push(source);
        }
      }
    });
  }

  onDataUpdated(index: number) {
    let penetration = 0;
    this.sources.forEach(source => {
      if (source.enabled) {
        penetration += source.amounts[source.selected].value;
      }
    });
    this.totalPenetration = penetration + 100;
    this.remainingBossArmor = 18200 - Math.min(this.totalPenetration, 18200);
  }

  onPenetrationTypeChanged(type: string) {
    this.selectedPenetrationSource = type;
    this.getPenetrationSources();
    this.onDataUpdated(0);
  }
}
