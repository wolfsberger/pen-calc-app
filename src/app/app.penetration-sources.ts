import { PenetrationSourceModel } from './components/penetration-source/penetration-source.model';

const PenetrationSources: PenetrationSourceModel[] = [
    {
        name: 'Crusher',
        stamina: true,
        magica: true,
        selected: 1,
        amounts: [
            {name: 'Default', value: 1622},
            {name: 'Infused', value: 2108},
            {name: 'Infused + Torugs', value: 2741}
        ],
        enabled: true,
        description: 'Gets applied by "Glyph of Crushing". The given values are for a golden CP160 glyph.'
    },
    {
        name: 'Sharpened Weapon',
        stamina: true,
        magica: true,
        selected: 0,
        amounts: [
            {name: 'Onehanded', value: 1376},
            {name: 'Twohanded', value: 2752}
        ],
        enabled: false
    },
    {
        name: 'Lover Mundus',
        stamina: true,
        magica: true,
        selected: 7,
        amounts: [
            {name: '0', value: 2752},
            {name: '1', value: 2958},
            {name: '2', value: 3164},
            {name: '3', value: 3371},
            {name: '4', value: 3578},
            {name: '5', value: 3784},
            {name: '6', value: 3990},
            {name: '7', value: 4196},
        ],
        enabled: false,
        description: 'Values are for zero to seven golden pieces of armor with the "Divines"\
         trait. This mundus stone can be found in Auridon, Glenumbra and Stonefalls'
    },
    {
        name: 'Minor Fracture',
        stamina: true,
        magica: false,
        selected: 0,
        amounts: [
            {name: 'Default', value: 1320}
        ],
        enabled: false,
        description: '',
        causedBy: [
            'Applied by Bow Ability: Focused Aim',
            'Templar Ability: Power of the Light',
            'Alchemy Poison/Potion: Lower Armor (Dragonthorn, Stinkhorn, Torchbug Thorax)',
            'Armor Set: Hand of Mephala (Loot)',
            'Armor Set: Sunderflame'
        ]
    },
    {
        name: 'Major Fracture',
        stamina: true,
        magica: false,
        selected: 0,
        amounts: [
            {name: 'Default', value: 5280}
        ],
        enabled: true,
        description: '',
        causedBy: [
            'One Hand and Shield Ability: Puncture+Morhps',
            'Nightblade Assassination Ability: Mark Target+Morphs',
            'Nightblade Shadow Ability: Surprise Attack (Morph of Veiled Strike)',
            'Dragonknight Ardent Flame Ability: Noxious Breath (Morph of Fiery Breath)',
            'Warden Animal Companion Ability: Subterranean Assault (Morph of Scorch)',
            'Armor Set: Night Mother\'s Gaze'
        ]
    },
    {
        name: 'Minor Breach',
        stamina: false,
        magica: true,
        selected: 0,
        amounts: [
            {name: 'Default', value: 1320}
        ],
        enabled: false,
        description: '',
        causedBy: [
            'Templar Dawn\'s Wrath Ability: Power of the Light (Morph of Backlash)',
            'Alchemy Poison/Potion: Lower Spell Resist (Beetle Scuttle, Lady\'s Smock, Violet Coprinus)',
            'Alchemy Poison: Increase Spell Resist (Bugloss, White Cap, Mudcrab Chitin)',
            'Armor Set: Sunderflame'
        ]
    },
    {
        name: 'Major Breach',
        stamina: false,
        magica: true,
        selected: 0,
        amounts: [
            {name: 'Default', value: 5280}
        ],
        enabled: true,
        description: '',
        causedBy: [
            'One Hand and Shield Ability: Pierce Armor (Morph of Puncture)',
            'Destruction Staff Ability: Weakness to Elements+Morphs',
            'Nightblade Assassination Ability: Mark Target+Morphs',
            'Warden Animal Companion Ability: Subterranean Assault (Morph of Scorch)',
            'Armor Set: Night Mother\'s Gaze'
        ]
    },
    {
        name: 'Concentration',
        stamina: false,
        magica: true,
        selected: 0,
        amounts: [
            {name: 'Default', value: 4884}
        ],
        enabled: true,
        description: 'Granted by the light armor passive "Concentration"'
    },
    {
        name: 'Penetrating Magic ',
        stamina: false,
        magica: true,
        selected: 0,
        amounts: [
            {name: 'Default', value: 1820}
        ],
        enabled: false,
        description: 'Gained by the Destruction Staff passive "Penetrating Magic" and is only valid for abillities of that tree.'
    },
    {
        name: 'Spinner',
        stamina: false,
        magica: true,
        selected: 0,
        amounts: [
            {name: 'Default', value: 3450}
        ],
        enabled: false,
        description: 'Granted by the five piece bonus of the Spinner\'s Garments Set'
    },
    {
        name: 'Flame Blossom',
        stamina: false,
        magica: true,
        selected: 0,
        amounts: [
            {name: 'Default', value: 1487}
        ],
        enabled: false,
        description: 'Granted by the four piece bonus of the Flame Blossom Set'
    },
    {
        name: 'Alkosh',
        stamina: true,
        magica: true,
        selected: 0,
        amounts: [
            {name: 'Default', value: 3010}
        ],
        enabled: false,
        description: 'Granted by the five piece bonus of the Roar of Alkosh set'
    },
    {
        name: 'Kragh',
        stamina: true,
        magica: false,
        selected: 0,
        amounts: [
            {name: 'Default', value: 1487}
        ],
        enabled: false,
        description: 'Granted by the one piece bonus of the Kra\'gh monster Set'
    },
    /*{
        name: 'Sunderflame',
        stamina: true,
        magica: false,
        selected: 0,
        amounts: [
            {name: 'Purple jewelry', value: 3360},
            {name: 'Golden jewelry', value: 3440},
        ],
        enabled: false,
        description: 'Granted by the five piece bonus of the Sunderflame Set'
    },*/
    /*{
        name: 'Night Mother\'s Gaze',
        stamina: true,
        magica: false,
        selected: 0,
        amounts: [
            {name: 'Default', value: 2580}
        ],
        enabled: false,
        description: 'Granted by the five piece bonus of the Night Mother\'s Gaze Set'
    },*/
    {
        name: 'Two Fanged Snake',
        stamina: true,
        magica: false,
        selected: 0,
        amounts: [
            {name: 'Default', value: 4200}
        ],
        enabled: false,
        description: 'Granted by the five piece bonus of the Two Fanged Snake Set'
    },
];

export default PenetrationSources;
